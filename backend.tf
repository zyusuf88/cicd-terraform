terraform {
  backend "s3" {
    bucket = "my-state-terraform-bucket"
    key    = "state"
    region = "us-east-1" 
    dynamodb_table = "backend"
  }
}

