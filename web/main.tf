# EC2 instance 
#using modules to transfer subnet and security group 

resource "aws_instance" "server" {
  ami = "ami-00beae93a2d981137"
  instance_type = "t2.micro"
  subnet_id = var.sn
  security_groups = [var.sg]

  tags = {
    Name = "myserver"
  }
}
