variable "sg" {
  description = "Security group"
  type = string
}

variable "sn" {
  description = "subnet ID"
  type = string
}